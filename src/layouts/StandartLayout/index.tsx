import { Outlet } from "react-router-dom";
import { Container } from "./styles";
import { Header } from "./components/Header";

export function StandartLayout() {
    return (
        <>
            <Container>
                <Header />
                <Outlet />
            </Container>
        </>
    );
}