import { Route, Routes } from "react-router-dom";
import { Home } from "./pages/Home";
import { Checkout } from "./pages/Checkout";
import { StandartLayout } from "./layouts/StandartLayout";

export function Router() {
    return (
        <Routes>
            <Route path="/" element={<StandartLayout />}>
                <Route path="/" element={<Home />} />
                <Route path="/checkout" element={<Checkout />} />
            </Route>
        </Routes>
    );
}