import { BrowserRouter } from "react-router-dom"
import { Router } from "./Router"
import { CartContextProvider } from "./contexts/CartContext"
import { ThemeProvider } from "styled-components"
import { standardTheme } from "./styles/themes/standardTheme"
import { GlobalStyle } from "./styles/global"

function App() {

  return (
    <>
      <ThemeProvider theme={standardTheme}>
        <BrowserRouter>
          <CartContextProvider>
            <Router />
          </CartContextProvider>
          <GlobalStyle />
        </BrowserRouter>
      </ThemeProvider>
    </>
  )
}

export default App
